from socket import *
import random
from common import *

def utf8len(s):
    return len(s.encode('utf-8'))

serverIP = "127.0.0.1"
serverPort = 11555
clientSocket = socket(AF_INET, SOCK_DGRAM)
clientSocket.connect((serverIP, serverPort))
while True:
    request = bytearray()
    domainName = input("Enter Domain Name: ")
    if(domainName.lower() == "end"):
        break
    # DNS Header
    request.extend(random.getrandbits(16).to_bytes(2, 'big')) # random id
    # flags qr, Opcode, AA, TC, RD,
    request.append(0b_0_0000_1_0_0)
    # flags RA, Z, RCODE
    request.append(0b_0_000_0000)
    request.extend(int(1).to_bytes(2, 'big')) # QDCOUNT
    request.extend(int(0).to_bytes(2, 'big')) # ANCOUNT
    request.extend(int(0).to_bytes(2, 'big')) # NSCOUNT
    request.extend(int(0).to_bytes(2, 'big')) # ARCOUNT
    
    # DNS Query
    request.extend(encodeQNAME(domainName)) #QNAME
    request.extend(int(1).to_bytes(2, 'big')) #QTYPE
    request.append(0) #QCLASS
    request.append(1)
    # Send request
    clientSocket.send(request)
    
    # wait for response
    response, serverAddress = clientSocket.recvfrom(2048)
    anCount = int().from_bytes(response[6:8], 'big') # read number of answers
    rcode = int(response[3] & 15) # check if query fulslfilled
    if rcode == 0:
        byteOffset = 12 # skip header bytes
        for i in range(anCount):
            name = int().from_bytes(response[byteOffset:byteOffset + 2], 'big')
            byteOffset += 2
            recordType = int().from_bytes(response[byteOffset:byteOffset + 2], 'big')
            byteOffset += 2
            recordTypeName = "Invalid Type"
            if(recordType == 1):
                recordTypeName = "Type A"
            elif(recordType == 2):
                recordTypeName = "Type NS"
            elif(recordType == 3):
                recordTypeName = "Type MD"
            elif(recordType == 4):
                recordTypeName = "Type MF"
            classTypeName = "Invalid Class" 
            # we only support class == 1 (class IN)
            if(int().from_bytes(response[byteOffset:byteOffset + 2], 'big') == 1):
                classTypeName = "class IN"
            byteOffset += 2
            ttl = int().from_bytes(response[byteOffset:byteOffset + 4], 'big')
            byteOffset += 4
            rdlength = int().from_bytes(response[byteOffset:byteOffset + 2], 'big')
            byteOffset += 2
            ip = "{0}.{1}.{2}.{3}".format(int(response[byteOffset]), int(response[byteOffset+1]), int(response[byteOffset+2]), int(response[byteOffset+3]))
            byteOffset += 4
            print("{0}: {1}, {2}, TTL {3}, addr ({4}) {5}".format(domainName, recordTypeName, classTypeName, ttl, rdlength, ip))
    else:
        print(f"error code {rcode}, domain name referenced in query does not exist")
    
clientSocket.close()