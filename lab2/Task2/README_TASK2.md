## How to run task 2 code
1. Ensure that the code is running on python version `3.10.6` or higher.
2. The `common.py` file should be in the same directory as both the client and server files.
3. the server can be started by running `py -3 server.py`
4. the client can be started by running `py -3 client.py`
5. the server can resolve the domain names listed in the lab document, and the client can be ended by typing `end`