from socket import *
from common import *
serverIP = "127.0.0.1"
serverPort = 11555
serverSocket = socket(AF_INET, SOCK_DGRAM)
serverSocket.bind((serverIP, serverPort))
database = {
    "google.com":[1, 1, 260, [192, 165, 1, 1], [192, 165, 1, 10]],
    "youtube.com":[1, 1, 160, [192, 165, 1, 2]],
    "uwaterloo.ca":[1, 1, 160, [192, 165, 1, 3]],
    "wikipedia.org":[1, 1, 160, [192, 165, 1, 4]],
    "amazon.ca":[1, 1, 160, [192, 165, 1, 5]],
}

while True:
    message, clientAddress = serverSocket.recvfrom(2048)
    print("Request:")
    printHex(message) # print out the message
    # read the queried domain name
    decodedDomainName, byteOffset = decodeQNAME(messageBytes=message, byteOffset=12)

    response = bytearray()
    # Create DNS Header
    response.extend(message[:2]) # Response has same ID
    # flags qr, Opcode, AA, TC, RD,
    response.append(0b_1_0000_1_0_0)

    if decodedDomainName in database:
        # flags RA, Z, RCODE
        response.append(0b_1_000_0000)
        response.extend(int(0).to_bytes(2, 'big')) # QDCOUNT
        response.extend((len(database[decodedDomainName]) - 3).to_bytes(2, 'big')) #ANCOUNT
    else:
        # flags RA, Z, RCODE
        response.append(0b_1_000_0011) # error code in RCODE
        response.extend(int(0).to_bytes(2, 'big')) #QDCOUNT
        response.extend(int(0).to_bytes(2, 'big')) #ANCOUNT
    
    response.extend(int(0).to_bytes(2, 'big')) #NSCOUNT
    response.extend(int(0).to_bytes(2, 'big')) #ARCOUNT

    if decodedDomainName in database:
        # for each answer matching domain name
        for i in range(len(database[decodedDomainName]) - 3):
            # DNS Answer Section
            response.extend(int(0xc0_0c).to_bytes(2,'big')) # NAME
            response.extend(database[decodedDomainName][0].to_bytes(2, 'big')) # Type
            response.extend(database[decodedDomainName][1].to_bytes(2, 'big')) # Class
            response.extend(database[decodedDomainName][2].to_bytes(4, 'big')) # TTL
            response.extend(int(4).to_bytes(2, 'big')) # RDLENGTH
            ip = database[decodedDomainName][3 + i] # RDATA (the ip)
            response.append(ip[0])
            response.append(ip[1])
            response.append(ip[2])
            response.append(ip[3])
            

    print("Response:")
    printHex(response) # print the response
    serverSocket.sendto(response, clientAddress)
