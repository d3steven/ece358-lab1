def encodeQNAME(name):
    encodedName = bytearray()
    domainNameBytes = name.encode("utf-8") # get bytes of NAME
    added_bytes = 0
    numBytes = 0
    # encode the NAME using the scheme given in the Lab manual
    while(added_bytes < len(domainNameBytes)):
        if(numBytes == 0):
            # insert octet indicating following number of bytes (max 255)
            numBytes = min(0b_11111111, len(domainNameBytes) - added_bytes)
            encodedName.append(numBytes)
        encodedName.append(domainNameBytes[added_bytes]) # encode NAME as bytes
        added_bytes += 1
        numBytes -= 1
    encodedName.append(0) # octet indicating following number of bytes is 0 (end)
    return encodedName

def decodeQNAME(messageBytes, byteOffset):
    # read nbytes indicated by first octet
    numBytes = int(messageBytes[byteOffset])
    domainName = bytearray()
    byteOffset += 1
    # assume the NAME is encoded using format in lab manual
    while(numBytes > 0):
        # only keep charachter bytes
        domainName.append(messageBytes[byteOffset])
        numBytes -= 1
        byteOffset += 1
        if(numBytes == 0):
            # read number of characters in next chunk (max 255)
            numBytes = int(messageBytes[byteOffset])
            byteOffset += 1
    # return decoded string, and byte offset after end of NAME
    return (domainName.decode("utf-8"), byteOffset)


def printHex(arrayOfBytes: bytearray):
    # Print each charater in hex, with always two digits
    for i in range(len(arrayOfBytes)):
        print(arrayOfBytes[i].to_bytes(1, 'big').hex(), end=" ")
        if(i % 16 == 15):
            # newline after 16 hex pairs
            print("")
    print("\n")