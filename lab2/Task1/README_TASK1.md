## How to run Task 1 Code
1. Organize files: `helloWorld.html` should be in the same directory as the server, `helloWorldold.html` should be in a subfolder "old" in the same directory.
2. The server can be started by running: `py -3 webserver.py` and will listen for connections on `http://127.0.0.1:11555`.
3. To access `helloWorld.html`, use the URL `http://127.0.0.1:11555/helloWorld.html`, and to access `helloWorldold.html`, use the url `http://127.0.0.1:11555/old/helloWorldold.html`. Both urls can be accessed from a web browser.