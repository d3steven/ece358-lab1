from socket import *
import os
from email.utils import formatdate
import sys
import platform
from time import mktime
from datetime import datetime

def utf8len(s):
    return len(s.encode('utf-8'))

serverIP = "127.0.0.1"
serverPort = 11555
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind((serverIP, serverPort))
serverSocket.listen(1)
# we save these error case responses for readabillity. the {#} fields will be replaced with values.
# we return headers for connection type, date, server details, allowed methods for this server, message content, message length, and last modified
ERR_405 = "HTTP/1.1 405 Method Not Allowed\r\nConnection: Keep-Alive\r\nDate: {0}\r\nServer: {1}\r\nAllow: GET, HEAD\r\nContent-Type:text/html; charset=UTF-8\r\nContent-Length: 174\r\n\r\n<!DOCTYPE html><html><head><title>405 Forbidden</title></head><body><h1>405 Method Not Allowed</h1><p>Only GET and HEAD requests are accepted by this server</p></body></html>"
ERR_404_HEADER = "HTTP/1.1 404 Not Found\r\nConnection: Keep-Alive\r\nDate: {0}\r\nServer: {1}\r\nAllow: GET, HEAD\r\nContent-Type:text/html; charset=UTF-8\r\nContent-Length: {2}\r\n\r\n"
ERR_404_BODY = "<!DOCTYPE html><html><head><title>404 Not Found</title></head><body><h1>404 Not Found</h1><p>The requested page could not be found. {0} is not available on this server.</p></body></html>"
OK_200_HEADER = "HTTP/1.1 200 OK\r\nConnection: Keep-Alive\r\nDate: {0}\r\nServer: {1}\r\nLast-Modified: {2}\r\nContent-Length: {3}\r\nContent-Type: text/html\r\n\r\n"
while True:
    connectionSocket, addr = serverSocket.accept()
    HTTPDATA = connectionSocket.recv(2048)
    HTTPRequest = HTTPDATA.decode()
    # split header into lines
    HTTPRequestLines = HTTPRequest.split("\r\n")
    # check that this request has at least a "GET" type and 2 spaces.
    if(len(HTTPRequestLines[0]) < 5):
        continue
    # read the start-line (rfc2616)
    Method, what, version = HTTPRequestLines[0].split()
    # create format strings for current time and server versions according to rfc2616
    now = datetime.now()
    stamp = mktime(now.timetuple())
    date = formatdate(timeval=stamp, localtime=False, usegmt=True)
    server = f"Python/{sys.version_info[0]}.{sys.version_info[1]}.{sys.version_info[2]} {platform.system()}/{platform.version()}"
    # check if we can handle this request
    if(Method.upper() == "GET" or Method.upper() == "HEAD"):
        requestedFile = os.path.join(os.getcwd(), what.strip('/'))
        # check if the file the client requested is on this server.
        if(not os.path.isfile(requestedFile)):
            # file not present, so return a 404 message.
            body = ERR_404_BODY.format(what.strip('/'))
            response = ERR_404_HEADER.format(date, server, utf8len(body)) + body
            connectionSocket.send(response.encode("utf-8"))
            connectionSocket.close()
            continue # goto start of loop
        # read file and gile metadata
        lastModified = formatdate(timeval=os.path.getmtime(requestedFile), localtime=False, usegmt=True)
        content = ""
        contentLenght = 0
        with open(requestedFile, "r") as f:
            content = f.read()
            contentLenght = utf8len(content)
        # format the header
        header = OK_200_HEADER.format(date, server, lastModified, contentLenght)
        if(Method.upper() == "GET"):
            # return response header and file content for get
            connectionSocket.send((header + content).encode("utf-8"))
        else:
            # return response header only for head
            connectionSocket.send(header.encode("utf-8"))
    else:
        # if the request is not a get or head, we refuse to handle it (err 405 via rfc2616)
        connectionSocket.send(ERR_405.format(date, server).encode("utf-8"))
    
    connectionSocket.close()
