#!/bin/bash

for ((i = 25 ; i <= 95 ; i += 10)); do
    echo "rho = 0.$i"
	python3 ./q2.py "2000" "1000000" "2000" "0.$i"
done