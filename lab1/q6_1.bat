@echo off
FOR /L %%i IN (50,10,90) DO (
    echo "rho = 0.%%i"
    py -3 ./q2.py "8000" "1000000" "2000" "0.%%i" "50"
)
FOR /L %%i IN (0,10,50) DO (
    echo "rho = 1.%%i"
    py -3 ./q2.py "8000" "1000000" "2000" "1.%%i" "50"
)