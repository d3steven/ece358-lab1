import random
import math
from collections import deque
import sys


SIMULATION_TIME = 2000
C = 1000000
L = 2000
RHO = 0.95
K = float("inf")

if(len(sys.argv) >= 5):
    SIMULATION_TIME = int(sys.argv[1])
    C = int(sys.argv[2])
    L = int(sys.argv[3])
    RHO = float(sys.argv[4])
if(len(sys.argv) == 6):
    K = int(sys.argv[5])

LAMBDA_ARRIVAL_RATE = RHO*(C/L)
LAMBDA_OBSERVER_ARRIVAL_RATE = LAMBDA_ARRIVAL_RATE*5
LAMDA_PACKET_SIZE = 1/L
values = list()

class Arrival:
    def __init__(self, arrivalTime, size) -> None:
        self.arrivalTime = arrivalTime
        self.size = size

class Observer:
    def __init__(self, arrivalTime) -> None:
        self.arrivalTime = arrivalTime

def getExpRV(_lambda):
    return (-1/_lambda) * math.log((1.0-random.uniform(0, 1)), math.e)


# generating arrival events
lastArrivalEvent = 0
while True:
    lastArrivalEvent += getExpRV(LAMBDA_ARRIVAL_RATE)
    if(lastArrivalEvent > SIMULATION_TIME): break
    packetSize = getExpRV(LAMDA_PACKET_SIZE)
    values.append(Arrival(lastArrivalEvent, packetSize))

# generating observer events
lastObserverEvent = 0
while True:
    lastObserverEvent += getExpRV(LAMBDA_OBSERVER_ARRIVAL_RATE)
    if(lastObserverEvent > SIMULATION_TIME): break
    values.append(Observer(lastObserverEvent))


values.sort(key = lambda x: x.arrivalTime)

nArrivals = 0
nDepartures = 0
nObservers = 0
buffer = deque()
idleSum = 0
seenEventSum = 0
lostSum = 0

lastDepartureTime = 0

for event in values:
    # departures
    while(len(buffer) > 0 and buffer[0] < event.arrivalTime):
        buffer.popleft()
        nDepartures += 1
    
    if type(event) == Arrival:
        nArrivals += 1
        processingTime = event.size/C
        lastDepartureTime = processingTime + max(lastDepartureTime, event.arrivalTime)
        if len(buffer) < K:
            buffer.append(lastDepartureTime)
        else:
            lostSum += 1
    else:
        nObservers += 1
        seenEventSum += len(buffer)
        if len(buffer) == 0:
            idleSum += 1

# generatedBuffer = 

# lastArrivalEvent = 0
# lastObserverEvent = 0
# while True:
    
# print("E[n]", seenEventSum/nObservers)
print(seenEventSum/nObservers)
# print("P idle", idleSum/nObservers)
# print(idleSum/nObservers)
# print("P loss", lostSum/nArrivals)
print(lostSum/nArrivals)
