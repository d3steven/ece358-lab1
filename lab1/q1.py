import random
import math
import numpy

NUM_TO_GEN = 1000
LAMBDA = 75
values = list()

# Function for expontential random variables
def getExpRV():
    return (-1/LAMBDA) * math.log((1-random.random()), math.e)

# Generate 1000 random variables
for _ in range(NUM_TO_GEN):
    values.append(getExpRV())

print("Mean:", numpy.mean(values))
print("Variance:", numpy.var(values, ddof=1))
