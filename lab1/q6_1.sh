#!/bin/bash

for ((i = 50 ; i <= 90 ; i += 10)); do
    echo "rho = 0.$i"
	python3 ./q2.py "2000" "1000000" "2000" "0.$i" "50"
done
for ((i = 0 ; i <= 50 ; i += 10)); do
    echo "rho = 1.$i"
	python3 ./q2.py "000" "1000000" "2000" "1.$i" "50"
done